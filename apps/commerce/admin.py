from django.contrib import admin
from apps.commerce.models import Product, Category, Promotion


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "created_by", "promotion", "in_stock"]
    list_display_links = ["id", "title"]
    readonly_fields = ["created_at", "updated_at"]
    search_fields = ["title"]
    ordering = ["-created_at"]
    actions_on_bottom = True

    def has_add_permission(self, request):
        return


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "turn_on"]
    list_display_links = ["id", "title"]
    search_fields = ["title"]
    ordering = ["turn_on"]
    actions_on_bottom = True
    list_editable = ["turn_on"]

    def has_add_permission(self, request):
        return


@admin.register(Promotion)
class PromotionAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "percent"]
    list_display_links = ["id", "title"]
    search_fields = ["title"]
    actions_on_bottom = True

    def has_add_permission(self, request):
        return
