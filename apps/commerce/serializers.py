from rest_framework import serializers
from apps.commerce import services, models


class CategoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ["id", "title", "turn_on"]
        read_only_fields = ["turn_on"]


class PromotionModelSerializer(serializers.ModelSerializer):
    percent = serializers.IntegerField(min_value=0, max_value=100)

    class Meta:
        model = models.Promotion
        fields = ["id", "title", "percent"]


class ProductModelSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(
        queryset=services.CategoryService.get_filter(turn_on=True)
    )
    discounted_price = serializers.IntegerField()

    class Meta:
        model = models.Product
        fields = [
            "id", "title", "image", "category",
            "in_stock", "promotion", "created_by",
            "price", "discounted_price",
        ]

    def perform_create(self, serializer):
        serializer.save(created_by=self.context["request"].user)
