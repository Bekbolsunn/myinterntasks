from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status
from apps.commerce.models import Category, Product


from apps.users.choices import ROLE_CHOICES
from apps.users.models import User


# class ProductAPITest(APITestCase):
#     def setUp(self):
#         self.user = User.objects.create_user(email='testuser@mail.com', username='testuser', password='testpassword', role='tenant')
#         self.refresh_token = RefreshToken.for_user(self.user)
#         self.access_token = str(self.refresh_token.access_token)
#         self.category = Category.objects.create(title='Example Category')  # Создаем категорию
#
#     def test_create_product(self):
#         url = reverse('product-list-create')
#         data = {
#             'title': 'new idea',
#             'role': 'tenant',
#             'price': 1200,
#             'promotion': 12,
#             'category': 1,
#             # 'created_by': self.user.id
#         }
#         headers = {'HTTP_AUTHORIZATION': f'Bearer {self.access_token}'}
#         print(f'ID : {self.category.id}')
#         response = self.client.post(url, data, format='json', **headers)
#
#         print(response.data)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)  # Проверка успешного создания


# class CategoryTests(APITestCase):
#     def setUp(self):
#         # for _ in range(2):  # Создадим 10 категорий
#         Category.objects.create(title='Unique Mamdda')
#
#     def test_category_list(self):
#         response = self.client.get(reverse('category-list'), {'ordering': 'id'})
#         print(response.data)
