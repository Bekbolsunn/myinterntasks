from apps.commerce import models
from apps.common.services import global_all_objects
from django.db.models import F



class ProductService:
    __product_model = models.Product

    def get_or_none(cls, *args, **kwargs):
        try:
            return cls.__product_model.objects.get(*args, **kwargs)
        except cls.__product_model.DoesNotExist:
            return None

    @classmethod
    def get_all(cls, *args, **kwargs):
        return cls.__product_model.objects.all()

    @classmethod
    def get_filter(cls, *args, **kwargs):
        return cls.__product_model.objects.filter(*args, **kwargs).select_related('promotion').annotate(
                discounted_price = F('price') - (F('price') * F('promotion__percent') / 100))

    # @staticmethod
    # def discounted_price(product):
    #     if product.promotion:
    #         discounted_price = product.price - (product.price * product.promotion.percent / 100)
    #         return discounted_price
    #     return product.price


class CategoryService:
    __category_model = models.Category

    def get_or_none(cls, *args, **kwargs):
        try:
            return cls.__category_model.objects.get(*args, **kwargs)
        except cls.__category_model.DoesNotExist:
            return None

    @classmethod
    def get_all(cls):
        return cls.__category_model.objects.all()

    @classmethod
    def get_filter(cls, *args, **kwargs):
        return cls.__category_model.objects.filter(*args, **kwargs)


class PromotionService:
    __promotion_model = models.Promotion

    @classmethod
    def get_or_none(cls, *args, **kwargs):
        try:
            return cls.__promotion_model.objects.get(*args, **kwargs)
        except cls.__promotion_model.DoesNotExist:
            return None

    @classmethod
    def get_all(cls):
        return cls.__promotion_model.objects.all()

    @classmethod
    def get_filter(cls, *args, **kwargs):
        return cls.__promotion_model.objects.filter(*args, **kwargs)
