from django.urls import path
from apps.commerce import views
from apps.common.views import GlobalSearchAPIView
from rest_framework import routers

router = routers.SimpleRouter()
router.register("categories", views.CategoryAPIViewSet, basename="category")
router.register("promotions", views.PromotionAPIViewSet, basename="promotion")

urlpatterns = [
    path("products/", views.ProductListCreateAPIView.as_view(), name="product-list-create"),
    path("products/<int:pk>/", views.ProductRetrieveUpdateDestroyAPIView.as_view(),
         name="product-update-delete"),
    path("global-search/", GlobalSearchAPIView.as_view(), name="global-search"),
]
urlpatterns += router.urls
