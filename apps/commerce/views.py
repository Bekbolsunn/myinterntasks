from rest_framework import viewsets, generics
from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend
from apps.commerce import serializers, filters, permissions, services


class CategoryAPIViewSet(viewsets.ModelViewSet):
    queryset = services.CategoryService.get_filter(turn_on=True)
    serializer_class = serializers.CategoryModelSerializer
    permission_classes = [permissions.IsAdminAndTenantPermission]


class PromotionAPIViewSet(viewsets.ModelViewSet):
    queryset = services.PromotionService.get_all()
    serializer_class = serializers.PromotionModelSerializer
    permission_classes = [permissions.IsAdminAndTenantPermission]


class ProductListCreateAPIView(generics.ListCreateAPIView):
    queryset = services.ProductService.get_filter(in_stock=True)
    serializer_class = serializers.ProductModelSerializer
    permission_classes = [permissions.IsAdminAndTenantCreatedByPermission]
    filter_backends = (
        SearchFilter,
        DjangoFilterBackend,
    )
    filterset_class = filters.ProductFilter
    search_fields = ["title"]

    def get_serializer_context(self):
        return {"request": self.request}


class ProductRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.ProductService.get_all()
    serializer_class = serializers.ProductModelSerializer
    permission_classes = [permissions.IsAdminAndTenantCreatedByPermission]
