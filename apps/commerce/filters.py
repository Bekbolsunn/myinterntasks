from apps.commerce.models import Product, Category
from django_filters import rest_framework as filters, ModelChoiceFilter


class ProductFilter(filters.FilterSet):
    category = ModelChoiceFilter(queryset=Category.objects.all())

    class Meta:
        model = Product
        fields = {"price": ["lt", "gt"]}
