import csv
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from apps.commerce.models import Product, Category, Promotion, User


class Command(BaseCommand):
    help = "Import data from CSV file"

    def add_arguments(self, parser):
        parser.add_argument("csv_file", help="Path to the CSV file")

    def import_data_from_csv(self, file_path):
        with open(file_path, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                title = row['title']
                price = int(row['price'])
                in_stock = row['in_stock'].lower() == 'true'
                created_by_id = row['created_by']
                promotion_id = row['promotion']
                category_id = row['category']
                print(row)

                if Product.objects.filter(title=title).exists():
                    self.stdout.write(self.style.WARNING(f"Продукт с таким названием' {title}' уже существует"))
                    continue

                try:
                    created_by = User.objects.get(id=created_by_id)
                except ObjectDoesNotExist:
                    self.stdout.write(self.style.ERROR(f"Пользователь '{created_by_id}' не существует. Пропуск."))
                    continue

                try:
                    category = Category.objects.get(id=category_id)
                except ObjectDoesNotExist:
                    self.stdout.write(self.style.ERROR(f"Категория '{category_id}' не существует. Пропуск."))
                    continue

                promotion = None
                if promotion_id:
                    try:
                        promotion = Promotion.objects.get(id=promotion_id)
                    except ObjectDoesNotExist:
                        self.stdout.write(self.style.ERROR(f"Акция '{promotion_id}' не существует. Пропуск."))
                        continue

                product = Product(
                    title=title,
                    price=price,
                    in_stock=in_stock,
                    created_by=created_by,
                    promotion=promotion,
                    category=category,
                )
                product.save()
                self.stdout.write(self.style.SUCCESS(f"Imported product: {title}"))

    def handle(self, *args, **options):
        csv_file = options["csv_file"]
        self.import_data_from_csv(csv_file)

# команда для запуска
# python manage.py import_data data/products.csv
