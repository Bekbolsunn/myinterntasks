from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAdminAndTenantCreatedByPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return bool(
            request.user.is_authenticated
            and (
                request.user.role == "tenant"
                or (request.method != "POST" and request.user.role == "admin")
            )
        )

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if not request.user.is_authenticated:
            return False

        if request.user.role == "admin":
            return True

        if request.user.role == "tenant" and obj.created_by == request.user:
            return True

        return False


class IsAdminAndTenantPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return bool(
            request.user.is_authenticated
            and (request.method == "POST" and request.user.role == "tenant")
        )
