from apps.users import models, google, utils


class UserService:
    __user_model = models.User

    @classmethod
    def email_exists(cls, email):
        return cls.__user_model.objects.filter(email=email).exists()

    @classmethod
    def get_or_none(cls, *args, **kwargs):
        try:
            cls.__user_model.objects.get(args, **kwargs)
        except:
            cls.__user_model.objects.none()

    @classmethod
    def get_all(cls):
        return cls.__user_model.objects.all()

    @classmethod
    def get_filter(cls, **kwargs):
        return cls.__user_model.objects.filter(**kwargs)

    @classmethod
    def create_user(cls, **kwargs):
        return cls.__user_model.objects.create_user(**kwargs)


class AuthService:
    @classmethod
    def validate_auth_token(cls, auth_token):
        user_data = google.Google.validate(auth_token)
        email = user_data["email"]
        name = user_data["name"]
        return utils.register_social_user(email, name)
