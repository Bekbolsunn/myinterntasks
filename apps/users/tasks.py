from celery import shared_task
from django.core.mail import send_mail
from config.settings.env_reader import env
from apps.users.models import User
from apps.commerce.models import Promotion
from celery.utils.log import get_task_logger
from django.template import loader

logger = get_task_logger(__name__)

@shared_task
def send_promotional_emails():
    users = User.objects.all()
    promotions = Promotion.objects.all()

    for user in users:
        subject = f"Привет мой бомж {user.username}"
        to_email = user.email

        context = {
            'user': user,
            'promotions': promotions,
        }
        html_message = loader.render_to_string('email_template.html', context)

        try:
            logger.info(f'Sending email to {to_email}')
            send_mail(
                subject=subject,
                message='',
                from_email=env("EMAIL_HOST_USER"),
                recipient_list=[to_email],
                html_message=html_message,
            )
            logger.info('Email sent')
        except Exception as e:
            logger.error(f'Error sending email to {to_email}: {str(e)}')
