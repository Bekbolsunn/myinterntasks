from django.urls import path
from apps.users import views
from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView
from django.contrib.auth.views import LogoutView

urlpatterns = [
    # пользователи
    path("users/", views.UserListAPIView.as_view(), name="list-users"),
    path("users/<int:pk>/", views.UserRetrieveUpdateDestroyAPIView.as_view(), name="get-user"),

    # вошел - вышел
    path("register/", views.RegistrationAPIView.as_view(), name="register"),
    path("login/", TokenObtainPairView.as_view(), name="token_access"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("logout", LogoutView.as_view(), name="logout"),

    # авторизация or рагистрация google
    path("google/", views.GoogleSocialAuthView.as_view(), name="token"),
]
