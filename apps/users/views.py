from apps.users import serializers, permissions, services
from rest_framework import generics, status, response, permissions as ps


class UserListAPIView(generics.ListAPIView):
    queryset = services.UserService.get_all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]


class UserRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = services.UserService.get_all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAdminOrReadOnly]


class RegistrationAPIView(generics.CreateAPIView):
    serializer_class = serializers.RegistrationSerializer
    permission_classes = [ps.AllowAny]


class GoogleSocialAuthView(generics.GenericAPIView):
    serializer_class = serializers.GoogleSocialAuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_data = serializer.validated_data
        return response.Response(user_data, status=status.HTTP_200_OK)
