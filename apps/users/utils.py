from apps.users.models import User
from config.settings.env_reader import env
from django.contrib.auth import authenticate
from rest_framework_simplejwt.tokens import RefreshToken


def register_social_user(email, name):
    filtered_user_by_email = User.objects.filter(email=email)

    if filtered_user_by_email.exists():
        registered_user = authenticate(email=email, password=env("SOCIAL_SECRET"))
        if registered_user:
            refresh = RefreshToken.for_user(registered_user)
            return {
                "message": "Вы авторизованы",
                "username": registered_user.username,
                "email": registered_user.email,
                "tokens": {
                    "refresh": str(refresh),
                    "access": str(refresh.access_token),
                },
            }
    else:
        user = {"username": name, "email": email, "password": env("SOCIAL_SECRET")}
        user = User.objects.create_user(**user)
        user.is_verified = True
        user.save()

        new_user = authenticate(email=email, password=env("SOCIAL_SECRET"))
        if new_user:
            refresh = RefreshToken.for_user(new_user)
            return {
                "message": "Вы зареганы и авторизованы",
                "email": new_user.email,
                "username": new_user.username,
                "tokens": {
                    "refresh": str(refresh),
                    "access": str(refresh.access_token),
                },
            }
