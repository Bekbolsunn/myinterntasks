from django.contrib import admin
from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = ["username", "email", "role",
              "is_active", "is_staff", 'is_superuser',
              "date_joined", "last_login", "password"]
    readonly_fields = ['password', "date_joined", "last_login"]
    list_display = ["id", "username", "email", "role", "is_active"]
    list_display_links = ["id", "username"]
    search_fields = ["username", "email"]
    ordering = ["-date_joined", "is_active", "is_staff"]
    actions_on_bottom = True
    list_editable = ["role"]
