from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from apps.users import managers, choices


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True, verbose_name="почта")
    username = models.CharField(max_length=30, verbose_name="имя")
    is_active = models.BooleanField(default=True, verbose_name="активен")
    is_staff = models.BooleanField(default=False, verbose_name="работник")
    date_joined = models.DateTimeField(
        default=timezone.now, verbose_name="дата регистрации"
    )
    role = models.CharField(
        max_length=50, choices=choices.ROLE_CHOICES, default="buyer"
    )

    objects = managers.UserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
        db_table = "users_user"
