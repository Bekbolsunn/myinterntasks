from django.contrib import admin
from apps.vote.models import Vote, Months
from django.db.models import Count


@admin.register(Months)
class MonthsAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']
    list_display_links = ['id', 'title']


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ["id", "category", "promotion", "product", "user", 'month']
    list_display_links = ["id", "category", "promotion", "product", "user", 'month']
    list_filter = ['category', "promotion", "product", "user", 'month']
    actions_on_bottom = True
    change_list_template = 'admin/vote_change_list.html'

    def has_add_permission(self, request):
        return

    def has_change_permission(self, request, obj=None):
        return

    def top_categories(self, obj):
        top_categories = (
                             Vote.objects
                             .values('category__title')
                             .annotate(total_votes=Count('category'))
                             .exclude(total_votes=0)
                             .order_by('-total_votes')
                         )[:5]
        return [(category['category__title'], category['total_votes']) for category in top_categories]

    top_categories.short_description = 'Популярные категории'

    def top_promotions(self, obj):
        top_promotions = (
                             Vote.objects
                             .values('promotion__title')
                             .annotate(total_votes=Count('promotion'))
                             .exclude(total_votes=0)
                             .order_by('-total_votes')
                         )[:5]
        return [(promotion['promotion__title'], promotion['total_votes']) for promotion in top_promotions]

    top_promotions.short_description = 'Популярные акции'

    def top_products(self, obj):
        top_products = (
                           Vote.objects
                           .values('product__title')
                           .annotate(total_votes=Count('product'))
                           .exclude(total_votes=0)
                           .order_by('-total_votes')
                       )[:5]
        return [(product['product__title'], product['total_votes']) for product in top_products]

    top_products.short_description = 'Популярные товары'

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}

        selected_month_id = request.GET.get('month')

        if not selected_month_id:
            selected_month_id = Months.objects.latest('id').id

        months = Months.objects.all()

        total_category_votes = (
                                   Vote.objects
                                   .filter(month_id=selected_month_id)  # Фильтрация по выбранному месяцу
                                   .aggregate(total_votes=Count('category'))
                               )['total_votes'] or 0

        total_promotion_votes = (
                                    Vote.objects
                                    .filter(month_id=selected_month_id)  # Фильтрация по выбранному месяцу
                                    .aggregate(total_votes=Count('promotion'))
                                )['total_votes'] or 0

        total_product_votes = (
                                  Vote.objects
                                  .filter(month_id=selected_month_id)  # Фильтрация по выбранному месяцу
                                  .aggregate(total_votes=Count('product'))
                              )['total_votes'] or 0

        extra_context['total_category_votes'] = total_category_votes
        extra_context['total_promotion_votes'] = total_promotion_votes
        extra_context['total_product_votes'] = total_product_votes

        extra_context['popular_categories'] = self.top_categories(None)
        extra_context['popular_promotions'] = self.top_promotions(None)
        extra_context['popular_products'] = self.top_products(None)

        extra_context['selected_month'] = int(selected_month_id)
        extra_context['months'] = months

        return super().changelist_view(request, extra_context=extra_context)
