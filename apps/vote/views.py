from rest_framework import generics, permissions, response, status
from apps.vote import serializers, models, services


class VoteCreateAPIView(generics.CreateAPIView):
    queryset = models.Vote.objects.all()
    serializer_class = serializers.VoteSerializer
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, month=models.Months.objects.latest('id'))
