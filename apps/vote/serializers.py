from rest_framework import serializers
from apps.vote.models import Vote, Months


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ['category', 'promotion', 'product']

    def validate(self, data):
        user = self.context['request'].user
        last_month_vote = Vote.objects.filter(user=user, month=Months.objects.latest('id'))

        if last_month_vote.exists():
            raise serializers.ValidationError("Пользователь уже голосовал в текущем месяце")

        return data
