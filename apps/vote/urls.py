from django.urls import path
from apps.vote import views

urlpatterns = [
    path("", views.VoteCreateAPIView.as_view(), name="vote-create"),
]
