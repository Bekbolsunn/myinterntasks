from django.db import models
from apps.commerce.models import Product, Promotion, Category
from apps.users.models import User


class Months(models.Model):
    title = models.CharField(blank=True, max_length=500, unique=True)

    def __str__(self) -> str:
        return self.title

    class Meta:
        db_table = 'vote_months'
        verbose_name_plural = "Месяц"
        verbose_name = "Месяц"


class Vote(models.Model):
    category = models.ForeignKey(to=Category, on_delete=models.CASCADE, null=True,
                                 related_name="vote_category", verbose_name="категория", )
    promotion = models.ForeignKey(to=Promotion, on_delete=models.CASCADE, null=True,
                                  related_name="vote_promotion", verbose_name="акция", )
    product = models.ForeignKey(to=Product, on_delete=models.CASCADE, null=True,
                                related_name="vote_product", verbose_name="продукт", )
    user = models.ForeignKey(to=User, on_delete=models.CASCADE,
                             related_name="vote_user", verbose_name="пользователь", )
    month = models.ForeignKey(to=Months, on_delete=models.CASCADE,
                              related_name="vote_months", verbose_name="месяц", )

    def __str__(self) -> str:
        return f'{self.user} {self.month}'

    class Meta:
        db_table = "vote_vote"
        verbose_name_plural = "Голосования"
        verbose_name = "Голосование"
