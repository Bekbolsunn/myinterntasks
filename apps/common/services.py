from django.db.models import Q
from django.core.cache import cache
from apps.commerce import serializers, services


# Global All Models Service
def global_all_objects(model):
    return model.objects.all()


class GlobalSearchService:
    @staticmethod
    def search_title(request):
        title = request.query_params.get('title', '')

        cached_data = cache.get(title)
        if not cached_data:
            product_results = services.ProductService.get_filter(Q(title__istartswith=title))
            category_results = services.PromotionService.get_filter(Q(title__istartswith=title))
            promotion_results = services.PromotionService.get_filter(Q(title__istartswith=title))
            print([i.__dict__ for i in product_results])

            serialized_data = {
                'products': serializers.ProductModelSerializer(product_results, many=True).data,
                'categories': serializers.CategoryModelSerializer(category_results, many=True).data,
                'promotions': serializers.PromotionModelSerializer(promotion_results, many=True).data
            }
            cache.set(title, serialized_data)

            return serialized_data

        return cached_data
