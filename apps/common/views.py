from rest_framework import views, response
from apps.common.services import GlobalSearchService
from redis.exceptions import ConnectionError


class GlobalSearchAPIView(views.APIView):

    def get(self, request):
        try:
            searched_data = GlobalSearchService.search_title(request)
            return response.Response(searched_data)
        except ConnectionError as e:
            return response.Response({"message": "Ошибка соединения с Redis: {}".format(str(e))}, status=500)
