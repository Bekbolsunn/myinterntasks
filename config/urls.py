from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
    path("v1/commerce/", include("apps.commerce.urls")),
    path("v1/users/", include("apps.users.urls")),
    path("v1/vote/", include("apps.vote.urls")),
]

urlpatterns += [path("i18n/", include("django.conf.urls.i18n"))]
if settings.DEBUG:
    import debug_toolbar

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
