from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.base")

celery = Celery('apps.users')

celery.conf.broker_connection_retry_on_startup = True

celery.config_from_object('django.conf:settings', namespace='CELERY')

celery.autodiscover_tasks()


