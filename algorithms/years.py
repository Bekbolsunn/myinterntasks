while True:
    year = int(input("Введите год: "))
    if year < 0:
        print("Нужно больше")
    elif year % 100 in [11, 12, 13, 14]:
        print(f"{year} лет")
    elif year % 10 == 1:
        print(f"{year} год")
    elif 2 <= year % 10 <= 4:
        print(f"{year} года")
    else:
        print(f"{year} лет")


# def correct_year(year):
#
#     if 0 <= year <= 1 or year > 20 and str(year)[-1] == '1':
#         return f'{year} год'
#
#     elif (year < 5 or year > 20) and str(year)[-1] in ['2', '3', '4']:
#         return f'{year} года'
#
#     else:
#         return f'{year} лет'
#
#
# print(correct_year(int(input('Введите год: '))))
